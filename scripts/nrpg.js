/* globals
CONFIG,
game,
renderTemplate
*/
"use strict";

import { log } from "./util.js";
import { MODULE_ID, FLAGS, LABELS } from "./const.js";
import { getSetting, SETTINGS } from "./settings.js";

export const PATCHES_nrpg = {};
PATCHES_nrpg.nrpg = {};

// ----- NOTE: Hooks ----- //

/**
 * Hook renderItemSheetNrpg to add template configuration options for jutsu.
 * @param {ItemSheetNrpg} sheet
 * @param {Object} html
 * @param {Object} data
 */
function renderItemSheetNrpgHook(app, html, data) {
  if (data.itemType !== "Jutsu") return;
  renderNrpgJutsuTemplateConfig(app, html, data);
}

/**
 * Hook dnd pre-item usage, to add the last targeted token to the options passed through.
 */
function nrpgpreUseItemHook(item, config, options) {
  foundry.utils.mergeObject(options.flags, { lastTargeted: game.user.targets.ids.at(-1) });
}

/**
 * Hook dnd template creation from item, so item flags regarding the template can be added.
 */
function nrpgUseItemHook(item, config, options, templates) { // eslint-disable-line no-unused-vars
  log("nrpg.useItem hook", item);
  if ( !templates || !item ) return;

  // Add item flags to the template(s)
  const attachToken = item.getFlag(MODULE_ID, FLAGS.ATTACHED_TOKEN.Jutsu_TEMPLATE);
  for ( const templateD of templates ) {
    const shape = templateD.t;
    let wallsBlock = item.getFlag(MODULE_ID, FLAGS.WALLS_BLOCK);
    let wallRestriction = item.getFlag(MODULE_ID, FLAGS.WALL_RESTRICTION);
    if ( !wallsBlock
      || wallsBlock === LABELS.GLOBAL_DEFAULT ) wallsBlock = getSetting(SETTINGS.DEFAULT_WALLS_BLOCK[shape]);
    if ( !wallRestriction || wallRestriction === LABELS.GLOBAL_DEFAULT ) {
      wallRestriction = getSetting(SETTINGS.DEFAULT_WALL_RESTRICTION[shape]);
    }

    if ( attachToken ) {
      switch ( attachToken ) {
        case "caster": {
          const token = item.parent.token ?? item.parent.getActiveTokens()[0];
          if ( token ) templateD.object.attachToken(token);
          break;
        }
        case "target": {
          const tokenId = options.flags.lastTargeted;
          if ( tokenId ) templateD.object.attachToken(tokenId);
          break;
        }
      }
    }
    templateD.setFlag(MODULE_ID, FLAGS.WALLS_BLOCK, wallsBlock);
    templateD.setFlag(MODULE_ID, FLAGS.WALL_RESTRICTION, wallRestriction);

    if ( item.getFlag(MODULE_ID, FLAGS.ADD_TOKEN_SIZE) ) {
      // Does the template originate on a token? (Use the first token found.)
      const templateOrigin = new PIXI.Point(templateD.x, templateD.y);
      const token = canvas.tokens.placeables.find(t => templateOrigin.almostEqual(t.center));
      if ( token ) {
        // Add 1/2 token size to the template distance.
        const { width, height } = token.document;
        const size = Math.min(width, height) * canvas.dimensions.distance;
        templateD.updateSource({ distance: templateD.distance + size });
      }
    }
  }
}

PATCHES_nrpg.nrpg.HOOKS = {
  renderItemSheetNrpg: renderItemSheetNrpgHook,
  ["nrpg.useItem"]: nrpgUseItemHook,
  ["nrpg.preUseItem"]: nrpgpreUseItemHook
};

/**
 * Inject html to add controls to the measured template configuration:
 * 1. Switch to have the template be blocked by walls.
 *
 * templates/scene/template-config.html
 */
async function renderNrpgJutsuTemplateConfig(app, html, data) {
  // By default, rely on the global settings.
  if (typeof data.document.getFlag(MODULE_ID, FLAGS.WALLS_BLOCK) === "undefined") {
    data.document.setFlag(MODULE_ID, FLAGS.WALLS_BLOCK, LABELS.GLOBAL_DEFAULT);
  }

  if (typeof data.document.getFlag(MODULE_ID, FLAGS.WALL_RESTRICTION) === "undefined") {
    data.document.setFlag(MODULE_ID, FLAGS.WALL_RESTRICTION, LABELS.GLOBAL_DEFAULT);
  }

  // Set variable to know if we are dealing with a template
  const areaType = data.system.target.type;
  data.isTemplate = areaType in CONFIG.NRPG.areaTargetTypes;
  data.walledtemplatesn5e = {
    blockoptions: LABELS.Jutsu_TEMPLATE.WALLS_BLOCK,
    walloptions: LABELS.Jutsu_TEMPLATE.WALL_RESTRICTION,
    attachtokenoptions: LABELS.Jutsu_TEMPLATE.ATTACH_TOKEN
  };

  const template = `modules/${MODULE_ID}/templates/walled-templates-nrpg-jutsu-template-config.html`;
  const myHTML = await renderTemplate(template, data);

  html.find(".form-group.consumption").first().after(myHTML);
}
