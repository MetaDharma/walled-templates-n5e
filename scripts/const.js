/* globals
duplicate
*/

"use strict";

export const MODULE_ID = "walledtemplatesn5e";

export const FLAGS = {
  WALLS_BLOCK: "wallsBlock",
  WALL_RESTRICTION: "wallRestriction",
  RECURSE_DATA: "recurseData",
  ATTACHED_TOKEN: {
    ID: "attachedTokenId",
    DELTAS: "attachedTokenDelta",
    Jutsu_TEMPLATE: "attachToken"
  },
  HIDE: {
    BORDER: "hideBorder",
    HIGHLIGHTING: "hideHighlighting"
  },
  HEIGHT_ALGORITHM: "heightAlgorithm",
  HEIGHT_CUSTOM_VALUE: "heightCustomValue",
  HEIGHT_TOKEN_OVERRIDES: "attachedTokenOverridesHeight",
  ADD_TOKEN_SIZE: "addTokenSize"
};

export const LABELS = {
  WALLS_BLOCK: {
    unwalled: "walledtemplatesn5e.MeasuredTemplateConfiguration.unwalled",
    walled: "walledtemplatesn5e.MeasuredTemplateConfiguration.walled",
    recurse: "walledtemplatesn5e.MeasuredTemplateConfiguration.recurse"
  },

  WALL_RESTRICTION: {
    light: "WALLS.Light",
    move: "WALLS.Movement",
    sight: "WALLS.Sight",
    sound: "WALLS.Sound"
  },

  Jutsu_TEMPLATE: {},

  GLOBAL_DEFAULT: "globalDefault"
};

LABELS.Jutsu_TEMPLATE.WALLS_BLOCK = duplicate(LABELS.WALLS_BLOCK);
LABELS.Jutsu_TEMPLATE.WALL_RESTRICTION = duplicate(LABELS.WALL_RESTRICTION);
LABELS.Jutsu_TEMPLATE.WALLS_BLOCK.globalDefault = "walledtemplatesn5e.MeasuredTemplateConfiguration.globalDefault";
LABELS.Jutsu_TEMPLATE.WALL_RESTRICTION.globalDefault = "walledtemplatesn5e.MeasuredTemplateConfiguration.globalDefault";
LABELS.Jutsu_TEMPLATE.ATTACH_TOKEN = {
  na: "walledtemplatesn5e.nrpg-jutsu-config.attach-token.na",
  caster: "walledtemplatesn5e.nrpg-jutsu-config.attach-token.caster",
  target: "walledtemplatesn5e.nrpg-jutsu-config.attach-token.target"
};

export const NOTIFICATIONS = {
  NOTIFY: {
    ATTACH_TOKEN_NOT_SELECTED: "walledtemplatesn5e.notifications.attach-last-selected-token",
    ATTACH_TOKEN_NOT_TARGETED: "walledtemplatesn5e.notifications.attach-last-targeted-token"
  }
};

export const ACTIVE_EFFECT_ICON = `modules/${MODULE_ID}/assets/ruler-combined-solid-gray.svg`;

export const SHAPE_KEYS = ["circle", "cone", "ray", "rect"];
